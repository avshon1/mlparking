//
//  ModelResult.swift
//  MLParking
//
//  Created by Andrei Shonbin on 24.07.2021.
//

import Foundation

struct MappingModel: Identifiable {
    let id = UUID()
    let confidence: Float
    let description: String
    let predictTime: Double
    let resizeTime: Double
}

class MapperResultModel {
    class func map(
        _ mapping: [String: String],
        confidence: [Float],
        predictTime: Double,
        resizeTime: Double
    ) -> [MappingModel] {
        var result: [MappingModel] = []
        for (idx, element) in confidence.enumerated() {
            guard let description = mapping[String(idx)] else { continue }
            result.append(
                MappingModel(
                    confidence: element,
                    description: description,
                    predictTime: predictTime,
                    resizeTime: resizeTime
                )
            )
        }
        return result.sorted { m1, m2 in
            m1.confidence > m2.confidence
        }
    }
}
