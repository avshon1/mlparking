//
//  ViewModel.swift
//  MLParking
//
//  Created by Andrei Shonbin on 07.01.2022.
//

import Foundation
import Combine
import SwiftUI

enum StateView {
    case initial
    case select(source: SourceType)
    case result
}

final class PredictViewModel: ObservableObject {
    // input
    @Published var uiImage: UIImage?
    @Published var state: StateView = .initial
    
    // output
    @Published var mappingModels: [MappingModel] = []
    
    private var subscriptions = Set<AnyCancellable>()
    
    
    

    var source: SourceType = SourceType.album
    var image: Image?
    
    private let predictProvider: PredictProvider
    
    init(predictProvider: PredictProvider = PredictProvider()) {
        self.predictProvider = predictProvider
        
        setupSubscriptions()
    }
    
    private func setupSubscriptions() {
        $uiImage
            .compactMap { $0 }
            .sink { [weak self] in
                guard let self = self else { return }
                self.mappingModels = self.predictProvider.predict(uiImage: $0)
                self.state = .result
            }
            .store(in: &subscriptions)
    }
    
    func change(state: StateView) {
        switch state {
            case .initial:
                break
            case .select(let source):
                self.source = source
                self.state = state
            case .result:
                break
        }
        
        
    }
    

}
