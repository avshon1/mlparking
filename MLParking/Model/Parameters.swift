//
//  Parameters.swift
//  MLParking
//
//  Created by Andrei Shonbin on  22.07.2021.
//

import Foundation

struct Paramaters: Decodable {
    let mapping: [String: String]
    let input_shape: [Int]
    let output_shape: Int
    let preprocessing: Preproccessing
}

struct Preproccessing: Decodable {
    let normalize: Normalize
}

struct Normalize: Decodable {
    let step: Int
    let mean: [Float32]
    let std: [Float32]
}


