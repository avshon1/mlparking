//
//  ModelParametersMapper.swift
//  MLParking
//
//  Created by Andrei Shonbin on  23.07.2021.
//

import Foundation

final class ModelParametersMapper {
    
    /// Create array for torch module
    /// - Parameter model: dto json of model
    /// - Returns: array NSNumber 0..n input_shape(number photo, color(number of colors), W, H), output_shape( number of predict class), mean(RGB), std(RGB)
    class func map(_ model: Paramaters) -> [NSNumber] {
        var result: [NSNumber] = []
        let input_shape = model.input_shape.map {NSNumber(value: $0)}
        let mean = model.preprocessing.normalize.mean.map {NSNumber(value: $0)}
        let std = model.preprocessing.normalize.std.map {NSNumber(value: $0)}
        result.append(contentsOf: input_shape)
        result.append(NSNumber(value: model.output_shape))
        result.append(contentsOf: mean)
        result.append(contentsOf: std)
        
        return result
    }
}
