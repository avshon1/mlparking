//
//  PredictProvider.swift
//  MLParking
//
//  Created by Andrei Shonbin on  19.07.2021.
//

import Foundation
import UIKit.UIImage
import Combine

final class PredictProvider: ObservableObject {

    var parameters: Paramaters?

    
    init() {
        parameters = parseJson()
    }
    
    private func parseJson() -> Paramaters {
        if let filePath = Bundle.main.path(forResource: "model_desc", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: filePath), options: Data.ReadingOptions.mappedIfSafe)
                let decoder = JSONDecoder()
                let parameters = try decoder.decode(Paramaters.self, from: data)
                
                return parameters
            } catch {
                fatalError("parsing model parameters error: \(error).")
            }
        }
        fatalError("Unexpected error.")
    }
    
    
    private lazy var module: TorchModule = {
        if let filePath = Bundle.main.path(forResource: "model_d", ofType: "pt"),
           let module = TorchModule(fileAtPath: filePath) {
            return module
        } else {
            fatalError("Can't find the model file!")
        }
    }()
    
    func predict(uiImage: UIImage?) -> [MappingModel] {
        let startResizeTime = CFAbsoluteTimeGetCurrent()
        guard
            let parametersModel = parameters,
            let resizedImage = uiImage?.resized(
                to: CGSize(
                    width: parametersModel.input_shape[2],
                    height: parametersModel.input_shape[3]
                )
            ),
            var pixelBuffer = resizedImage.normalized(
                mean: parametersModel.preprocessing.normalize.mean,
                std: parametersModel.preprocessing.normalize.std
            )
        else { return []}
        let resizeTime = CFAbsoluteTimeGetCurrent() - startResizeTime
        let startTime = CFAbsoluteTimeGetCurrent()
        
        guard let outputs = module.predict(
            image: &pixelBuffer,
            parameters: ModelParametersMapper.map(parametersModel)
        )
        else { return [] }
        
        let output_res = outputs.map {$0.floatValue}
        if !output_res.isEmpty {
            return MapperResultModel.map(
                parametersModel.mapping,
                confidence: softMax(output_res),
                predictTime: CFAbsoluteTimeGetCurrent() - startTime,
                resizeTime: resizeTime
            )
           
        }
        return []
    }
    
    func softMax(_ outputs: [Float]) -> [Float] {
        let exp_outputs = outputs.map {exp(Double($0))}
        let sumExp = exp_outputs.reduce(0, +)
        return exp_outputs.map {Float($0 / sumExp)}
    }
}
