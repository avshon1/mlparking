//
//  CaptureImageView.swift
//  MLParking
//
//  Created by Andrei Shonbin on  17.07.2021.
//

import SwiftUI

enum SourceType {
    case camera
    case album
}

struct CaptureImageView {
   var viewModel: PredictViewModel
    
    func makeCoordinator() -> Coordinator {
      return Coordinator(viewModel: viewModel)
    }
}

extension CaptureImageView: UIViewControllerRepresentable {
    func makeUIViewController(
        context: UIViewControllerRepresentableContext<CaptureImageView>
    ) -> UIImagePickerController {
        let picker = UIImagePickerController()
        let sourceTypePhoto = viewModel.source == .album ? UIImagePickerController.SourceType.photoLibrary : UIImagePickerController.SourceType.camera
        picker.sourceType = sourceTypePhoto
        picker.allowsEditing = false
        picker.delegate = context.coordinator
        return picker
    }
    
    func updateUIViewController(
        _ uiViewController: UIImagePickerController,
        context: UIViewControllerRepresentableContext<CaptureImageView>
    ) {
//        print("updateUIViewController = \(context.coordinator.uiImage)")
        
    }
}
