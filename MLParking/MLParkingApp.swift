//
//  MLParkingApp.swift
//  MLParking
//
//  Created by Andrei Shonbin on  17.07.2021.
//

import SwiftUI

@main
struct MLParkingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: PredictViewModel())
        }
    }
}
