//
//  ResultCellView.swift
//  MLParking
//
//  Created by Andrei Shonbin on 24.07.2021.
//

import SwiftUI

struct ResultCellView: View {
    var mappingModel: MappingModel
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("\(mappingModel.description) (confidence: \(mappingModel.confidence))")
                .font(.subheadline)
                .foregroundColor(.secondary)
        }
    }
}

//struct ResultCellView_Previews: PreviewProvider {
//    static var previews: some View {
//        ResultCellView()
//    }
//}
