//
//  ResultView.swift
//  MLParking
//
//  Created by Andrei Shonbin on  19.07.2021.
//

import SwiftUI

struct ResultView: View {
    @ObservedObject var viewModel: PredictViewModel
    
    var body: some View {
        VStack{
            viewModel.image?.resizable()
                    .aspectRatio(contentMode: .fit)
                    .shadow(radius: 10)
                    .padding()
                Divider()
            if viewModel.mappingModels.count > 0 {
                Text("\(viewModel.mappingModels[0].description) (\(viewModel.mappingModels[0].confidence))\nspend time: \(viewModel.mappingModels[0].predictTime + viewModel.mappingModels[0].resizeTime)\npredict time: \(viewModel.mappingModels[0].predictTime) sec\ntransform image time: \(viewModel.mappingModels[0].resizeTime)")
                Divider()
            }
            Text("Full result:")
                .italic()
            List{
                ForEach(viewModel.mappingModels) {
                    ResultCellView(mappingModel: $0)
                }
            }
        }
    }
}

//struct ResultView_Previews: PreviewProvider {
//    static var previews: some View {
//        ResultView()
//    }
//}
