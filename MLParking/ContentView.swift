//
//  ContentView.swift
//  MLParking
//
//  Created by Andrei Shonbin on  17.07.2021.
//

import SwiftUI
import UIKit


struct ContentView: View {
    
    @ObservedObject var viewModel: PredictViewModel
    
    var body: some View {
        
        ZStack {
            VStack {
                HStack {
                    Button("Select photo") {
                        viewModel.change(state: .select(source: .album))
                    }
                    .foregroundColor(.green)
                    Spacer()
                    if UIImagePickerController.isSourceTypeAvailable(.camera){
                        Button("Make photos") {
                            viewModel.change(state: .select(source: .camera))
                        }
                        .foregroundColor(.green)
                    }
                }
                .padding()
                Group{
                    switch viewModel.state {
                        case .select:
                            CaptureImageView(viewModel: viewModel)
                        case .initial:
                            Text("Application need to get any photo of parking.")
                        case .result:
                            ResultView(viewModel: viewModel)
                    }
                    Spacer()
                }
            }
        }
    }
}
