//
//  Coordinator.swift
//  MLParking
//
//  Created by Andrei Shonbin on  17.07.2021.
//

import SwiftUI

class Coordinator: NSObject,
    UINavigationControllerDelegate,
    UIImagePickerControllerDelegate
{
    var viewModel: PredictViewModel
 
    init(viewModel: PredictViewModel) {
        self.viewModel = viewModel
    }
 
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]
    ) {
        guard let unwrapImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        viewModel.uiImage = unwrapImage
        viewModel.image = Image(uiImage: unwrapImage)
        viewModel.state = .result
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        viewModel.state = .initial
    }
}
